"use strict";

Template.form.events({
    'submit form': function (event) {
        event.preventDefault();

        var imageFile = event.currentTarget.children[0].files[0];

        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {
                //to-do: inform the user that it failed
            } else {
                //to-do: insert post data right here
                //insert data: name, message, imageId, created by date
                //fileObject._id./
                $('.grid').masonry('reloadItems');
            }
        });
    }
});